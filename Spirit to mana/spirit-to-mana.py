#!/bin/python3

def pick_class():
    #Input class and spirit and convert class input into lowercase
    print('What class?')
    class_pick = input().lower()
    print('How much spirit for calculation?')
    spirit = float(input())
    
    #List of classes and their respective spirit coefficients and base spirit
    #Druid-c is for druids when they are in Moonkin or normal form
    #Druid-f is for druids in cat/bear form
    class_dict = {'druid-c': [4.5, 15], 'druid-f': [5, 15], 'hunter': [5, 15], 'mage': [4, 12.5], 'paladin': [5, 15], 'priest': [4, 12.5], 'shaman': [5, 17], 'warlock': [5, 15]}

    #Check if class exists in the dictionary and return the values into the function, else terminate app
    if class_pick in class_dict:
        return calculate(spirit, class_dict[class_pick][0], class_dict[class_pick][1])
    else:
        print('Error! Wrong class.')
        quit()

def calculate(spirit, spirit_c, spirit_plus):
    #Formula to calculate mana regeneration
    mana = spirit_plus + (spirit / spirit_c)
    
    #Print out the mana regeneration for different statuses, talents and spells
    #Also round them up to 2 decimals
    print('\nRegeneration: ' + format(round(mana,2)) + ' mana per tick or ' + format(round(mana * 30,2)) + ' mana/minute.\n')
    print('15% mana regeneration: ' + format(round(mana * 15 / 100,2)) + ' mana/tick or ' + format(round(mana * 15 / 100 * 30,2)) + ' mana/minute.\n')
    print('30% mana regeneration: ' + format(round(mana * 30 / 100,2)) + ' mana/tick or ' + format(round(mana * 30 / 100 * 30,2)) + ' mana/minute.\n')
    print('45% mana regeneration: ' + format(round(mana * 45 / 100,2)) + ' mana/tick or ' + format(round(mana * 45 / 100 * 30,2)) + ' mana/minute.\n')
    print('Innervate: ' + format(round(mana * 4,2)) + ' mana per tick or ' + format(round(mana * 4 * 10,2)) + ' mana/20seconds.\n')
    print('Evocation: ' + format(round(mana * 15,2)) + ' mana per tick or ' + format(round(mana * 15 * 4,2)) + ' mana/8seconds.\n')

#Run the function
pick_class()